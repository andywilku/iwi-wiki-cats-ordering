package iwi_wiki;

import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.graph.*;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;

import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class GraphGenerator {
    private Hashtable<String,Node> NodesList;
    private Graph resultGraph;
    private boolean printLabels = false;
    private List<String> childNodes;
    private double graphZoomValue = 0.5;
    private int edgeCount = 0;

    public GraphGenerator(Hashtable<String, Node> nodesList, boolean printLabels,String fileName){
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        this.NodesList = nodesList;
        this.printLabels = printLabels;
        resultGraph = new SingleGraph("IWI-Projekt");
        edgeCount = 0;
        generateGraph();
        if(printLabels) printLabels(fileName);
        displayGraph();
    }

    private void displayGraph() {
        resultGraph.display();

        Viewer viewer = resultGraph.display();
        View view = viewer.getDefaultView();
        view.getCamera().setViewPercent(graphZoomValue);
    }

    private void generateGraph() {
        Set<String> keys = NodesList.keySet();
        String leafNode;


        for (String key : keys)
            addNode(NodesList.get(key));

        for (String key : keys) {
            childNodes = NodesList.get(key).getChildren();
            leafNode = NodesList.get(key).getID();

            for(String child : childNodes){
                if(!child.isEmpty()){
                    edgeCount++;
                    addEdge(leafNode,child);
                }

            }
        }
        System.out.println("ILOSC KRAWEDZI: " + edgeCount);
    }

    private void printLabels(String filename){
        for(org.graphstream.graph.Node n:resultGraph) {
            n.addAttribute("ui.label", n.getId());
            n.addAttribute("ui.style", "size: 1px;" );
            if(filename.contains("Orginal"))
                n.addAttribute("ui.style", "fill-color: red;" );
            else
                n.addAttribute("ui.style", "fill-color: green;" );

        }
    }

    private void addNode(Node node){
        resultGraph.addNode(node.getID());
    }

    private void addEdge(String leafNode, String parentNode){
        String temp = leafNode.concat(parentNode);
        resultGraph.addEdge(temp,leafNode,parentNode);
        //System.out.println("Added Edge: " + leafNode + "-" + parentNode); DEBUG INFO
    }
}
