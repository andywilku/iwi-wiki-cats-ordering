package iwi_wiki;

import java.util.*;

public class LineProcessor {

    private List<String> buffer;
    private int sum;
    private String id;
    private String[] splitted;
    private List<Node> nodeReference;

    public LineProcessor(){
        nodeReference = new ArrayList<Node>();
    }

    public void processChildNodes(List<String> childNodes){
        for(String node: childNodes){
            splitted = node.split("\\t",2);
            id = splitted[0];

            createChildNode(id);
        }
    }

    public void processParentNodes(List<String> parentNodes){
        nodeReference = new ArrayList<Node>();

        for(String node: parentNodes) {
            sum = 0;
            splitted = node.split("\\t", 2);
            id = splitted[0];
            splitted = node.split("\\t");

            //for (int i = 1; i < splitted.length; i++)
              //  sum = sum + Integer.parseInt(splitted[i]);

            insertIntoChildArray(splitted);
            createParentNode(id);
        }
    }

    public void assignParentsToChildren(Hashtable<String,Node> map){
        List<String> tempChildren;
        Set<String> keys = map.keySet();

        for (String key : keys){
            if(!map.get(key).isItLeafNode()) {
                tempChildren = map.get(key).getChildren();
                assignParent(map,tempChildren,key);
            }
        }
    }

    private void assignParent(Hashtable<String,Node> map, List<String> childID, String parentID){
        for (String child : childID) {
            map.get(child).setParent(parentID);
        }
    }

    private void insertIntoChildArray(String[] splitted){
        buffer = new ArrayList<String>();
        for(int i = 1; i < splitted.length; i++){
            if(!splitted[i].isEmpty() && splitted[i] != "0");
                buffer.add(splitted[i]);
        }
    }

    private void createChildNode(String id){
        Node node = new Node(id,true);
        nodeReference.add(node);
    }

    private void createParentNode(String id){
        Node node = new Node(id);
        for(int i = 0; i < buffer.size(); i++)
            node.addChildren(buffer.get(i));

        nodeReference.add(node);
    }

    public List<Node> getCreatedNode(){
        return nodeReference;
    }
}