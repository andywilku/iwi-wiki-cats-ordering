package iwi_wiki;

import java.util.Hashtable;

public class GlobalNodeList {
    private Hashtable<String, Node> map;

    public GlobalNodeList(){
         map = new Hashtable<>();
    }

    public Node getNode(String id){
        return map.get(id);
    }

    public void addNode(String id, Node node){
        map.put(id,node);
        //System.out.println("Added a node to a global list: " +id);
    }

    public void removeNode(String id){
        map.remove(id);
    }

    public Hashtable<String, Node> getHashtable(){
        return map;
    }




}
