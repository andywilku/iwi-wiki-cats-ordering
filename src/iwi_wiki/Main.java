package iwi_wiki;

import java.util.*;
import java.lang.Integer;
import java.io.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.Map.Entry;


public class Main {
    final static String OUTPUT_FILE_PATH = "src/matrixOut.txt";
    final static String INPUT_FILE_PATH = "src/matrixOutOrginal.txt";


    public static void main(String[] args) throws IOException {
        Andrzej();
        processOutputAndPrepareGraph(INPUT_FILE_PATH);
        processOutputAndPrepareGraph(OUTPUT_FILE_PATH);
    }

    private static void processOutputAndPrepareGraph(String fileName) throws FileNotFoundException {

        GlobalNodeList nodeList;
        NodeTypeDivider nodeDivider;
        LineProcessor lp;
        List<Node> processedleafNodes;
        List<Node> processedparentNodes;
        Hashtable<String, Node> map;
        List<String> buffer = new ArrayList<String>();

        lp = new LineProcessor();
        nodeDivider = new NodeTypeDivider(fileName);
        List<String> leafNodes = nodeDivider.getLeafNodes();
        List<String> parentNodes = nodeDivider.getParentNodes();
        nodeList = new GlobalNodeList();

        lp.processChildNodes(leafNodes);
        processedleafNodes = lp.getCreatedNode();

        lp.processParentNodes(parentNodes);
        processedparentNodes = lp.getCreatedNode();

        displayProcessedLeafNodes(processedleafNodes,nodeList);
        displayProcessedParentNodes(processedparentNodes,nodeList);

        map = nodeList.getHashtable();
        lp.assignParentsToChildren(map);

        //checkParents(map); DEBUG INFO
        GraphGenerator graph = new GraphGenerator(map,true,fileName);
    }

    private static void displayProcessedLeafNodes(List<Node> processedleafNodes, GlobalNodeList nodeList){
        System.out.println("__________________________________");
        for(Node node : processedleafNodes){
            System.out.println("Leaf node: "+node.getID());
            nodeList.addNode(node.getID(),node);
        }
    }


    private static void displayProcessedParentNodes(List<Node> processedParentNodes, GlobalNodeList nodeList){

        for(Node node : processedParentNodes){
            System.out.println("Parent node: "+node.getID() + " with following childrens: " + node.getChildren());
            nodeList.addNode(node.getID(),node);

        }
        System.out.println("__________________________________");
    }

    private static void checkParents(Hashtable<String, Node> map){
        Set<String> keys = map.keySet();
        for (String key : keys)
            System.out.println("Parent of ID: " + map.get(key).getID() + " is: " + map.get(key).getParent());
    }

    private static void Andrzej() throws IOException {
        int index = 0;
        int sizeOfRow = 19; //543-dla matrixFull; 19-dla mediaMatrix; 99-dla articlesPartMatrix; 32-dla wikipediaPartMatrix
        int nextTmpValue;
        int nextTranslateValue;
        String nextTranslateValue1;

        Hashtable<Integer, List<Integer>> map = new Hashtable<>();
        Hashtable<Integer, List<Integer>> mapOrginal = new Hashtable<>();
        Hashtable<String, List<Integer>> mapAfterTranslate = new Hashtable<>();
        Hashtable<String, List<Integer>> mapOrginalAfterTranslate = new Hashtable<>();
        ArrayList<Integer> categoryStructureMatrix = new ArrayList<>();
        ArrayList<Integer> tableFirsCol = new ArrayList<>();
        Hashtable<Integer, String> translatorMap = new Hashtable<>();

        //Wczytywanie macierzy z pliku
        Scanner matrixFile = new Scanner(new File("src/mediaMatrix.txt")).useDelimiter(",");
        Scanner translator = new Scanner(new File("src/media-cattitle.txt")).useDelimiter(";");

        //Wpisanie kolejno do listy
        while(matrixFile.hasNext())
        {
            categoryStructureMatrix.add(Integer.parseInt(matrixFile.next()));
            if(index%sizeOfRow==0 )    //543
            {
                tableFirsCol.add(categoryStructureMatrix.get(index));
            }
            index++;
        }

        while(translator.hasNext())
        {
            nextTranslateValue1 = translator.next();
            nextTranslateValue = Integer.parseInt(translator.next());


            translatorMap.put(nextTranslateValue,nextTranslateValue1);

        }

        Iterator<Integer> matrixIterator = tableFirsCol.iterator();
        index = 0;
        while(matrixIterator.hasNext())
        {
            nextTmpValue = matrixIterator.next();
            if (nextTmpValue != 0)
            {
                final int myFinalVariable = nextTmpValue;
                //Zapisywanie pozycji dla poszczególnych indeksów kategorii
                List<Integer> allIndexes = IntStream.range(0, categoryStructureMatrix.size()).boxed()
                        .filter(i -> categoryStructureMatrix.get(i).equals(myFinalVariable))
                        .collect(Collectors.toList());
                List<Integer> allIndexesOrginal = IntStream.range(0, categoryStructureMatrix.size()).boxed()
                        .filter(i -> categoryStructureMatrix.get(i).equals(myFinalVariable))
                        .collect(Collectors.toList());
                mapOrginal.put(nextTmpValue,allIndexesOrginal);
                map.put(nextTmpValue,allIndexes);
                //System.out.println("Values before cleaning: " + map.get(nextTmpValue));
                while(map.get(nextTmpValue).size()>2)
                {
                    List<Integer> categoriesPositions = map.get(nextTmpValue);
                    int nextTmpListValue=0;
                    Iterator<Integer> listIter = categoriesPositions.iterator();
                    //Porządkowanie. Usuwanie cykli.
                    while(listIter.hasNext())
                    {
                        int a = listIter.next();
                        if(nextTmpListValue<a && a%sizeOfRow!=0)
                        {
                            nextTmpListValue = a;
                        }
                    }
                    map.get(nextTmpValue).remove(categoriesPositions.indexOf(nextTmpListValue));
                }

            }
            index++;
        }

        //tutaj przeba przetłumaczyć hashmape
        Iterator it = translatorMap.entrySet().iterator();
        Iterator itMap = map.entrySet().iterator();
        for (Entry<Integer,String> entry : translatorMap.entrySet()) {
            int k = entry.getKey();
            String v = entry.getValue();
            for (Entry<Integer, List<Integer>> entry1 : map.entrySet()) {
                int k1 = entry1.getKey();
                List<Integer> v1 = entry1.getValue();
                if (k == k1){
                    mapAfterTranslate.put(v,v1);
                }
            }
        }

        Iterator itOrginal = translatorMap.entrySet().iterator();
        Iterator itMapOrginal = mapOrginal.entrySet().iterator();
        for (Entry<Integer,String> entry : translatorMap.entrySet()) {
            int k = entry.getKey();
            String v = entry.getValue();
            for (Entry<Integer, List<Integer>> entry1 : mapOrginal.entrySet()) {
                int k1 = entry1.getKey();
                List<Integer> v1 = entry1.getValue();
                if (k == k1){
                    mapOrginalAfterTranslate.put(v,v1);
                }
            }
        }

        ArrayList<String> Finall = new ArrayList<>(categoryStructureMatrix.size());
        ArrayList<String> FinallOrginal = new ArrayList<>(categoryStructureMatrix.size());
        int counter = 0;
        for(int cats:categoryStructureMatrix){
            Finall.add(counter, "0");
            counter++;
        }

        counter = 0;
        for(int cats:categoryStructureMatrix){
            FinallOrginal.add(counter, "0");
            counter++;
        }
        counter = 0;
        for (Map.Entry<String, List<Integer>> entry : mapAfterTranslate.entrySet())
        {
            List<Integer> value = entry.getValue();
            String key = entry.getKey();
            int val = value.get(0);
            int val2;

            if(value.size() != 2){
                Finall.set(val, key);
            }else {
                val2 = value.get(1);
                Finall.set(val, key);
                Finall.set(val2, key);
            }

            //System.out.println(key + " - " + value.get(0));
            counter++;
        }

        for (Map.Entry<String, List<Integer>> entry : mapOrginalAfterTranslate.entrySet())
        {
            List<Integer> value = entry.getValue();
            String key = entry.getKey();
            int val = value.get(0);
            int val2;
            int i=0;
            if(value.size() != 2){
                while(i<value.size()){
                    //val2 = value.get(1);
                    //FinallOrginal.set(val, key);
                    FinallOrginal.set(value.get(i), key);
                    i++;
                }
            }else {
                while(i<value.size()){
                    //val2 = value.get(1);
                    //FinallOrginal.set(val, key);
                    FinallOrginal.set(value.get(i), key);
                    i++;
                }
            }

            //System.out.println(key + " - " + value.get(0));
            counter++;
        }

        //Zapisanie bufora do plików
        FileWriter fileWriter = new FileWriter("src/matrixOut.txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);

        counter = 1;
        Iterator<String> finalIterator = Finall.iterator();
        while(finalIterator.hasNext()) {
            if (counter % sizeOfRow != 0 || counter == 1) {
                printWriter.print(finalIterator.next() + "\t");
            } else {
                printWriter.println(finalIterator.next() + "\t");
            }
            counter++;
        }
        printWriter.close();

        FileWriter fileWriterOrginal = new FileWriter("src/matrixOutOrginal.txt");
        PrintWriter printWriterOrginal = new PrintWriter(fileWriterOrginal);

        counter = 1;
        Iterator<String> finalIteratorOrginal = FinallOrginal.iterator();
        while(finalIteratorOrginal.hasNext()) {
            if (counter % sizeOfRow != 0 || counter == 1) {
                printWriterOrginal.print(finalIteratorOrginal.next() + "\t");
            } else {
                printWriterOrginal.println(finalIteratorOrginal.next() + "\t");
            }
            counter++;
        }
        printWriterOrginal.close();

        System.out.println("Reading to table complete!");
    }
}
