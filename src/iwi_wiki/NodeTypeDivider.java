package iwi_wiki;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NodeTypeDivider {

    Scanner matrixFile;
    private List<String> leafNodes = new ArrayList<String>();
    private List<String> parentNodes = new ArrayList<String>();
    private boolean operationCompleted;

    public NodeTypeDivider(String filePath) throws FileNotFoundException {
        this.operationCompleted = false;
        matrixFile = new Scanner(new File(filePath));
        divideNodes();
    }

    public List<String> getLeafNodes(){
        return leafNodes;
    }

    public List<String> getParentNodes(){
        return parentNodes;
    }

    private void divideNodes(){
        int result = 0;

        while(matrixFile.hasNext()) {
            String line = matrixFile.nextLine();
            result = processLine(line);
            if(result > 0)
                insertIntoParentNodeType(line);
            else
                insertIntoChildNodeType(line);
        }
    }

    private void insertIntoChildNodeType(String line) {
        leafNodes.add(line);
    }

    private void insertIntoParentNodeType(String line) {
        parentNodes.add(line);
    }

    private int processLine(String line){
        //lineWithoutTabs = tabsIntoSpaces(line);
        String[] buffer = line.split("\\t");
        int sum = 0;

        for(int i = 1; i < buffer.length; i++){
            if(!buffer[i].isEmpty() && buffer[i] != "0")
                sum += 1;
        }

        return sum;
    }

//    private String tabsIntoSpaces(String line) {
//        Pattern ptn = Pattern.compile("\\s+");
//        Matcher mtch = ptn.matcher(str);
//        return mtch.replaceAll(replace);
//    }
}
